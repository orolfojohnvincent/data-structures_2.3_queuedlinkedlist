#include <iostream>
#include <cstdlib>
//#include <conio.h>
#include <string.h>

using namespace std;

static int maxi=0;
class List{
	private:
		typedef struct node{
			string title;
			string singer;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr tail;
		nodePtr temp;
		nodePtr curr;
	public:
		List();
		void Abq(string addTitle, string addSinger);
		void Bbq();
		void PrintList();
};

List::List()
{
	head = NULL;
	tail = NULL;
	temp = NULL;
	curr = NULL;
}

void List::Abq(string addTitle, string addSinger)
{
	nodePtr n = new node;
	n->title = addTitle;
	n->singer = addSinger;
	n->next = NULL;
	if(head!=NULL){
		curr=head;
		while (curr->next!=NULL){
			curr=curr->next;
		}
		curr->next = n;
		tail = n;
		maxi++;
	}
	else{
		head = n;
		tail = n;
		maxi++;
	}
	
}

void List::Bbq()
{
	if(head==NULL){
		cout<<"Queue is empty!\n";
	}
	else if(head->next == NULL){
		head = NULL;
		cout<<"Queue is now empty!\n";
		maxi--;
	}
	else{
		curr = head;
		head = head->next;
		curr->next = NULL;
		cout<<"Successfully Dequeued!\n";
		maxi--;
	}
}

void List::PrintList(){
	curr = head;
	int x = 1;
	//temp = curr->next;
	while (x!=maxi+1){
		cout<<"("<<x<<")\n";
		cout<<"Title: "<<curr->title<<"\n";
		cout<<"Singer: "<<curr->singer<<"\n";
		x++;
		curr = curr->next;
	}
}
bool Close(){
	return 0;
}

List yey;

void intro()
{
	system("cls");
	cout<<" 1. Enqueue\n 2. Dequeue\n 3. Display\n 4. Exit\n";
	int c;
	cin>>c;
	cin.ignore(1,'\n');
	if(c>4){
		cout<<"INVALID INPUT!\n\n";
		intro();
	}
	switch(c){
		case 1:{
			string addTitle, addSinger;
			cout<<"Enter title: ";
			getline(cin,addTitle);
			cout<<"Enter singer: ";
			getline(cin,addSinger);
			yey.Abq(addTitle,addSinger);
			cout<<"Music saved!\n";
			system("pause");
			intro();
			break;
		}
		case 2:{
			yey.Bbq();
			system("pause");
			intro();
			break;
		}
		case 3:{
			//Display
			yey.PrintList();
			system("pause");
			intro();
			break;
		}
		case 4:{
			cout<<"                               GOODBYE.... ";
			Close();
			break;
		}
	}
}

int main()
{
	yey.Abq("Kay Tagal", "Mark Carpio");
	yey.Abq("Walang Kwenta", "Bassilyo");
	yey.Abq("Dungaw", "Gloc-9");
	yey.Abq("Wonderful Sound", "Tom Jones");
	yey.Abq("Crazy", "Kenny Rogers"); 
	yey.Abq("Pulang Manok", "Unknown");
	yey.Abq("Goodbye","Air Supply");
	yey.Abq("Istokwa", "Lexus");   
	
	cout<<"           MUSIC PLAYER INITIALIZING...\n";
	for(int i=10; i<=100; i+=15){
		system("pause");
		if(i==100){
			system("cls");
			cout<<i<<"%\n";
			cout<<"OPENING...\n";
			system("pause");
			system("cls");
		}
		system("cls");
		cout<<i<<"%\n";
	}
	
	intro();
	return 0;
	
}
